# How to start
1. Clone this repository
```
git clone git@bitbucket.org:opelykh/qa.git
```
2. Open UI-automation folder
```
cd UI-automation
```
3. Run  command
```
npm i
```

# How to run tests
- Run all tests (each test which name ends with _.test.js_)
```
npm test mobile.test.js -- --runInBand --detectOpenHandles --no-cache
```
- Run specific test
```
npm test FILENAME.test.js
```

# Help
- [Puppeteer docs](https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#puppeteerlaunchoptions)
- [Jest docs](https://jestjs.io/docs/en/getting-started)
- [Jest Puppeteer examples](https://github.com/smooth-code/jest-puppeteer)
- [Jest expect library](https://jestjs.io/docs/en/expect)
