const puppeteer = require('puppeteer');
// import config from './src/config/config.js'

const BROWSER = {
  width: 1536,
  height: 809,
  slowMo: 100,
  headless: false
}

module.exports = async function() {
  let url = 'https://platform.cryptoexchangeranks.com';
  const width = BROWSER.width;
  const height = BROWSER.height;

  let page;

  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 50,
    args: [`--window-size=${width},${height}`],
    devtools: false
  });

  // store the browser instance so we can teardown it later
  // this global is only available in the teardown but not in TestEnvironments
  global.__BROWSER_GLOBAL__ = browser;
};
