import locators from '../locators.js';

export default class LiquidityDashboard {

  async checkPresenceOfWidgets(page) {
    await page.waitForSelector(locators.liquidityDashboard.currencyWidget.container);
    await page.waitForSelector(locators.liquidityDashboard.dailyTradingWidget.container);
    await page.waitForSelector(locators.liquidityDashboard.marketDepthWidget.container);
    await page.waitForSelector(locators.liquidityDashboard.quotesWidget.container);
    await page.waitForSelector(locators.liquidityDashboard.liquidityWidget.container);
    await page.waitForSelector(locators.liquidityDashboard.arbitrageWidget.container);
  }
}
