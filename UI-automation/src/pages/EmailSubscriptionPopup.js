import locators from '../locators.js';
import config from '../config/config.js'

class EmailSubscriptionPopup {

  async fillEmail(iframe) {
    let email = await iframe.$(locators.emailSubscriptionPopup.emailInput);
    await email.click();
    await email.type(config.userSubscribe.email);
  }

  async fillName(iframe) {
    let name = await iframe.$(locators.emailSubscriptionPopup.nameInput);
    await name.click();
    await name.type(config.userSubscribe.name);
  }

  async fillCountry(iframe) {
    let country = await iframe.$(locators.emailSubscriptionPopup.countryDropdown);
    await country.click();
    await country.type('Ukr');
  }

  async fillUserType(iframe) {
    let userType = await iframe.$(locators.emailSubscriptionPopup.userTypeDropdown);
    await userType.click();
    await userType.type('Ind');
  }

  async enableCheckbox(iframe) {
    let checkbox = await iframe.$(locators.emailSubscriptionPopup.checkbox);
    await checkbox.click();
  }

  async submitForm(iframe) {
    let submitButton = await iframe.$(locators.emailSubscriptionPopup.submitButton);
    await submitButton.click();
  }

}
export default new EmailSubscriptionPopup();
