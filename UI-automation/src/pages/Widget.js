import locators from '../locators.js';
import helpers from '../helpers.js';

class Widget {

  // list items
  async countWidgetListItems(page, widgetContainer) {
    await page.waitForSelector(`${widgetContainer} ${locators.widgetCommon.listItem}`);
    let widgetListItems = await page.$$(`${widgetContainer} ${locators.widgetCommon.listItem}`);
    widgetListItems = widgetListItems.length;
    return widgetListItems;
  }

  // description
  async openWidgetDescription(page, widgetContainer) {
    await page.click(`${widgetContainer} ${locators.widgetCommon.descriptionIcon}`);
  }

  // settings
  async openCloseSettings(page, widgetContainer) {
    await page.click(`${widgetContainer} ${locators.widgetCommon.settingsIcon}`);
    await page.waitForSelector(`${widgetContainer} ${locators.widgetCommon.settingsWrapper}`)
  }

  async choose3rdItemInSingleSelect(page, widgetContainer, settingLocator) {
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.inputArrow}`);
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.thirdDropdownItem}`)
  }

  async selectFirstSingleSelectItem(page, widgetContainer, settingLocator) {
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.inputArrow}`);
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.firstDropdownItem}`)
  }

  async changeBidAskSingleSelectItem(page, widgetContainer, settingLocator) {
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.inputArrow}`);
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.secondDropdownItem}`);
  }

  async disableMultiselectDropdownItems(page, widgetContainer, settingLocator, amountOfItemsToBeDisabled) {
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingMultiselect.inputArrow}`);
    for (let i = 0; i < amountOfItemsToBeDisabled; i++) {
      await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingMultiselect.firstDropdownItem}`);
    };
  }
// deprecated
  async countDropdownItemsInMultiselectDropdown(page, widgetContainer, settingLocator) {
    await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingMultiselect.inputArrow}`);
    let dropdowntItems = await page.$$(`${widgetContainer} ${settingLocator} ul>li`);
    return dropdowntItems.length;
  }

  async countDropdownItems(page, widgetContainer, settingLocator, type) {
    if (type === 'single') {
      await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingSingleSelect.inputArrow}`);
    } else {
      await page.click(`${widgetContainer} ${settingLocator} ${locators.widgetCommon.settingMultiselect.inputArrow}`);
    }
    let dropdownItems = await page.$$(`${widgetContainer} ${settingLocator} ul>li`);
    return dropdownItems.length;
  }

  async typeValueToInput(page, widgetContainer, settingLocator, value) {
    await page.type(`${widgetContainer} ${settingLocator} input`, value);
  }

  async clearInput(page, widgetContainer, settingLocator) {
    let textInput = locators.widgetCommon.textInput;
    await page.click(`${widgetContainer} ${settingLocator} ${textInput}`);
      await page.keyboard.press('Backspace');
  }

  // SPECIFIC
  // liquidity dashboard - liquidity widget
  async getFirstCellPrice(page, widgetContainer) {
    let cellLocator = `${widgetContainer} [name="widget-cell"]:nth-child(2)`;
    let priceInsideCell = await page.evaluate(
      (cellLocator) => {return document.querySelector(cellLocator).textContent}, cellLocator
    );
    return priceInsideCell;
  }

  // liquidity dashboard - market depth widget
  async getAvgBidPrice(page, widgetContainer) {
    let cellWithPriceLocator = `${widgetContainer} [name="chart-info"] li p span`
    let priceValue = await page.evaluate(
      (cellWithPriceLocator) => {return document.querySelector(cellWithPriceLocator).textContent}, cellWithPriceLocator
    );
    return priceValue;
  }

  // arbitrage dashboard - matrix view widgets
  async countEmptyCells(page, widgetContainer) {
    let anyCellLocator = `${widgetContainer} [name="widget-cell"]`;
    let emptyCellsAmount = await page.evaluate(function(anyCellLocator) {
      let counter = 0;
      let widgetCells = document.querySelectorAll(anyCellLocator);
      for (i = 0; i < widgetCells.length; i++) {
        if (widgetCells[i].textContent == '--') {
          counter++;
          }
      }
      return counter;
    }, anyCellLocator);
    return emptyCellsAmount;
  }
  // SPECIFIC - END

  // fullscreen
  async enableDisableFullscreen(page, widgetContainer) {
    await page.click(`${widgetContainer} ${locators.widgetCommon.fullscreenIcon}`);
  }

  async checkIfWidgetIsInFullscreen(page, widgetContainer) {
    let containerWithSize = `${widgetContainer} [name="widget"]`;
    let isWidgetInFullscreen = await page.evaluate(
        (containerWithSize) => {return document.querySelector(containerWithSize).style.height ==
         document.querySelector(containerWithSize).style.maxHeight}, containerWithSize
    );
    return isWidgetInFullscreen;
  }

  async measureCanvasSize(page, widgetContainer) {
    let canvasEl = `${widgetContainer} canvas`;
    let canvasSize =  await page.evaluate(
      (canvasEl) => {return parseInt(document.querySelector(canvasEl).style.height.split('p')[0])}, canvasEl
    );
    return canvasSize;
  }

  // sorting
  async checkSorting(page, widgetContainer, columnNumber) {
    let cell = `${widgetContainer} ${locators.widgetCommon.cell}`;
    // reset sorting
    await page.click(`${widgetContainer} ${locators.widgetCommon.head} li:nth-child(${columnNumber})`);
    // get first cell text
    let cellTextContent = await page.evaluate(
      (cell) => {return document.querySelector(cell).textContent}, cell
    );
    // change sorting
    await page.click(`${widgetContainer} ${locators.widgetCommon.head} li:nth-child(${columnNumber})`);
    // get first cell text
    let cellTextContentAfterSorting = await page.evaluate(
      (cell) => {return document.querySelector(cell).textContent}, cell
    );
    await expect(cellTextContentAfterSorting).not.toBe(cellTextContent);
  }

  // select item in widget list
  async selectWidgetListItem(page, widgetContainer, itemNumber) {
    await page.waitForSelector(locators.widgetCommon.listItem)
    await page.click(`${widgetContainer} [name="widget-list-item"]:nth-child(${itemNumber})`);
  }

  async checkIfWidgetListItemIsSelected(page, widgetContainer, itemNumber) {
    let listItemSelector = `${widgetContainer} [name="widget-list-item"]:nth-child(${itemNumber})`;
    let isListItemSelected = await page.evaluate(
      (listItemSelector) => {return (document.querySelector(listItemSelector).className).indexOf('active') !== -1}, listItemSelector
    );
    return isListItemSelected;
  }

}
export default new Widget();
