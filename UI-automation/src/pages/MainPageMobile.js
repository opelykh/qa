import puppeteer from "puppeteer";
import locators from '../locators.js';

class MainPageMobile {

  async openMenu(page) {
    await page.waitForSelector(locators.mobile.burgerMenuButton);
    await page.click(locators.mobile.burgerMenuButton);
    await page.waitForSelector(locators.mobile.menu.rating);
  }

  async openRatingWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.rating);
    await page.waitForSelector(locators.mobile.ratingWidget.container);
  }

  async openCurrencyWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.currency);
    await page.waitForSelector(locators.mobile.currencyWidget.container);
  }

  async openQuotesWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.quotes);
    await page.waitForSelector(locators.mobile.quotesWidget.container);
  }

  async openLiquidityWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.liquidity);
    await page.waitForSelector(locators.mobile.liquidityWidget.container);
  }

  async openBBwidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.bb);
    await page.waitForSelector(locators.mobile.bbWidget.container);
  }

  async openArbitrageWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.arbitrage);
    await page.waitForSelector(locators.mobile.arbitrageWidget.container);
  }

  async openDailyTradingWidget(page) {
    await this.openMenu(page);
    await page.click(locators.mobile.menu.dailyTrading);
    await page.waitForSelector(locators.mobile.dailyTradingWidget.container);
  }

  async openExternalURL(browser, page, locator, expectedURL, waitTime) {
    await page.click(locator);
    await helpers.wait(waitTime);
    let pages = await browser.pages();
    let newPage = pages[2];
    let url = await newPage.evaluate(() => location.href);
    await expect(url).toBe(expectedURL);
    await newPage.close();
  }

}

export default new MainPageMobile();
