import locators from '../locators.js';

export default class ScoringDashboard {
  async checkPresenceOfWidgets(page) {
    await page.waitForSelector(locators.scoringDashboard.ratingWidget.container);
    await page.waitForSelector(locators.scoringDashboard.scoringWidget.container);
    await page.waitForSelector(locators.scoringDashboard.exchangeProfileWidget.container);
  }

  // rating & exchange profile
  async getDataFromExchangeProfileWidget(page) {
    let elementWithTextLocator = `${locators.scoringDashboard.exchangeProfileWidget.container} i`;
    await page.waitForSelector(elementWithTextLocator);
    // save exchange profile widget data
    let exchangeProfileWidgetData = await page.evaluate(
      (elementWithTextLocator) => {return document.querySelector(elementWithTextLocator).textContent}, elementWithTextLocator
    );
    return exchangeProfileWidgetData;
  }

  // scoring
  async expandListItemAndCheckIfDetailsAppear(page) {
    let secondListItemLocator = `${locators.scoringDashboard.scoringWidget.container} ${locators.widgetCommon.secondListItem}`;
    let detailsSectionLocator = locators.scoringDashboard.scoringWidget.detailsContainer;
    // wait for list item
    await page.waitForSelector(secondListItemLocator);
    // check that details section is not displayed
    let isDetailsSectionPresent = await page.evaluate(
      (detailsSectionLocator) => {return document.querySelector(detailsSectionLocator) === null}, detailsSectionLocator
    );
    await expect(isDetailsSectionPresent).toBeTruthy();
    // click on list item
    await page.click(secondListItemLocator);
    // check if detaild container appears
    await page.waitForSelector(locators.scoringDashboard.scoringWidget.detailsContainer), {timeout: 1000};
  }

}
