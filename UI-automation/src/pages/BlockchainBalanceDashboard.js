import locators from '../locators.js';

export default class BlockchainBalanceDashboard {
  async checkPresenceOfWidgets(page) {
    await page.waitForSelector(locators.blockchainBalanceDashboard.BBhotWidget.container);
    await page.waitForSelector(locators.blockchainBalanceDashboard.BBcoldWidget.container);
    await page.waitForSelector(locators.blockchainBalanceDashboard.walletDetailsWidget.container);
    await page.waitForSelector(locators.blockchainBalanceDashboard.flowDynamicsWidget.container);
  }

  // works for wallet details and flow dynamics widgets
  async defineSelectedExchange(page, widgetContainer) {
    await page.waitForSelector(locators.blockchainBalanceDashboard.walletDetailsWidget.selectedExchangeNameInHeader);
    let exchangeNameInHeaderLocator = `${widgetContainer} ${locators.blockchainBalanceDashboard.walletDetailsWidget.selectedExchangeNameInHeader}`;
    let exchangeNameFromHeader = await page.evaluate(
      (exchangeNameInHeaderLocator) => {return document.querySelector(exchangeNameInHeaderLocator).textContent}, exchangeNameInHeaderLocator
    );
    return exchangeNameFromHeader;
  }
}
