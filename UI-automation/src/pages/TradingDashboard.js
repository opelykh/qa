import locators from '../locators.js';

export default class TradingDashboard {
  async checkPresenceOfWidgets(page) {
    await page.waitForSelector(locators.tradingDashboard.chartWidget.container), {timeout: 10000};
    await page.waitForSelector(locators.tradingDashboard.listWidget.container), {timeout: 10000};
    await page.waitForSelector(locators.tradingDashboard.marketDepthWidget.container), {timeout: 10000};
  }

  // Market depth
  async marketDepthGetCurrencyPairFromHeader(page) {
    let currencyPairLocator = await `${locators.tradingDashboard.marketDepthWidget.container} ${locators.tradingDashboard.marketDepthWidget.currencyPairInHeader}`;
    let currencyPair = await page.evaluate(
      (currencyPairLocator) => {return document.querySelector(currencyPairLocator).textContent}, currencyPairLocator
    );
    return currencyPair;
  }

  // Market depth
  async marketDepthGetExchangeFromHeader(page) {
    let exchangeLocator = await `${locators.tradingDashboard.marketDepthWidget.container} ${locators.tradingDashboard.marketDepthWidget.exchangeInHeader}`;
    let exchange = await page.evaluate(
      (exchangeLocator) => {return document.querySelector(exchangeLocator).textContent}, exchangeLocator
    );
    return exchange;
  }
}
