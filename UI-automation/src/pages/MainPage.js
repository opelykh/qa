import puppeteer from "puppeteer";
import locators from '../locators.js';
import ScoringDashboard from './ScoringDashboard.js';
import LiquidityDashboard from './LiquidityDashboard.js';
import BlockchainBalanceDashboard from './BlockchainBalanceDashboard.js';
import ArbitrageDashboard from './ArbitrageDashboard.js';
import TradingDashboard from './TradingDashboard.js';
import helpers from '../helpers.js';

class MainPage {

  async openDashboard(page, locator) {
    await page.click(locator);
  }

  async openDashboardDescription(page, dashboardLocator) {
    await page.waitForSelector(`${dashboardLocator} ${locators.header.dashboardTabs.descriptionIcon}`);
    await page.click(`${dashboardLocator} ${locators.header.dashboardTabs.descriptionIcon}`);
    await page.waitForSelector(locators.description.wrapper);
  }

  async closeDescription(page) {
    await page.click(locators.header.dashboardTabs.activeTab);
  }

  async openScoringDashboard(page) {
    await page.click(locators.header.dashboardTabs.scoring);
    return new ScoringDashboard();
  }

  async openLiquidityDashboard(page) {
    await page.click(locators.header.dashboardTabs.liquidity);
    return new LiquidityDashboard();
  }

  async openBlockchainBalanceDashboard(page) {
    await page.click(locators.header.dashboardTabs.blockchainBalance);
    return new BlockchainBalanceDashboard();
  }

  async openArbitrageDashboard(page) {
    await page.click(locators.header.dashboardTabs.arbitrage);
    return new ArbitrageDashboard();
  }

  async openTradingDashboard(page) {
    await page.click(locators.header.dashboardTabs.trading);
    return new TradingDashboard();
  }

  async openExternalURL(browser, page, locator, expectedURL, waitTime) {
    await page.click(locator);
    await helpers.wait(waitTime);
    let pages = await browser.pages();
    let newPage = pages[2];
    let url = await newPage.evaluate(() => location.href);
    await expect(url).toBe(expectedURL);
    await newPage.close();
  }

  async switchToDescriptionTab(page, locator) {
    await page.waitForSelector(locator);
    await page.click(locator);
  }
}

export default new MainPage();
