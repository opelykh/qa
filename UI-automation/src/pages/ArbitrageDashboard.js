import locators from '../locators.js';

export default class ArbitrageDashboard {
  async checkPresenceOfWidgets(page) {
    await page.waitForSelector(locators.arbitrageDashboard.listWidget.container);
    await page.waitForSelector(locators.arbitrageDashboard.matrixWidget.container);
    await page.waitForSelector(locators.arbitrageDashboard.listDetailWidget.container);
    await page.waitForSelector(locators.arbitrageDashboard.matrixDetailWidget.container);
  }

  // workd for list detail and matrix detail markets
  async getTextFromChart(page, widgetContainer) {
    let elementWithTextLocator = `${widgetContainer} ${locators.arbitrageDashboard.listDetailWidget.elementWithTextContent}`;
    let textContent = await page.evaluate(
      (elementWithTextLocator) => {return document.querySelector(elementWithTextLocator).textContent}, elementWithTextLocator
    );
    return textContent;
  }

  async clickOnNotEmptyCellOnMatrixWidget(page) {
    await page.click(`${locators.arbitrageDashboard.matrixWidget.container} ${locators.arbitrageDashboard.matrixWidget.notEmptyCell}`);
  }

  async clickOnSecondNotEmptyCellOnMatrixWidget(page) {
    let cellsLocator = `${locators.arbitrageDashboard.matrixWidget.container} ${locators.arbitrageDashboard.matrixWidget.notEmptyCell}`;
    let element = await page.evaluate(
      (cellsLocator) => {document.querySelectorAll(cellsLocator)[1].click()}, cellsLocator
    );
    await page.click(`${locators.arbitrageDashboard.matrixWidget.container} ${locators.arbitrageDashboard.matrixWidget.notEmptyCell}`);
  }
}
