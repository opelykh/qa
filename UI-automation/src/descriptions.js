export default 
{
	'dashboards': {
		'scoring': 'a[href="#/"]',
		'liquidity': 'a[href="#/liquidity-dashboard"]',
		'blockchain': 'a[href="#/blockchain-dashboard"]',
		'arbitrage': 'a[href="#/arbitrage-dashboard"]',
		'trading': 'a[href="#/trading-dashboard"]'
	}
}

// export default locators;