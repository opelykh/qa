export default
{
	header: {
		container: '#header',
		logo: '[name="logo"]',
		dashboardTabs: {
			scoring: 'a[href="#/"]',
			liquidity: 'a[href="#/liquidity-dashboard"]',
			blockchainBalance: 'a[href="#/blockchain-dashboard"]',
			arbitrage: 'a[href="#/arbitrage-dashboard"]',
			trading: 'a[href="#/trading-dashboard"]',
			descriptionIcon: 'div[name="description-icon"]',
			activeTab: '[aria-current="page"]'
		},
	},
	footer: {
		policies: '#footer [name="terms"]',
		logo: '[alt="footer logo"]',
		subscribeButton: '[name="subscribe-button"]',
		blogButton: '[name="blog"] a'
	},
	scoringDashboard: {
		ratingWidget: {
			container: '[name="rating-container"]',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)'
		},
		scoringWidget: {
			container: '[name="scoring-container"]',
			listItem: '[name="screen-list-head-child"] li',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			detailsContainer: '[name="detail-score"]'
		},
		exchangeProfileWidget: {
			container: '[name="scoring-charts-container"]'
		}
	},
	liquidityDashboard: {
		currencyWidget: {
			container: '[name="currency-container"]',
			settingCurrency: '[name="widget-menu"]',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]'
		},
		dailyTradingWidget: {
			container: '[name="dyily-trading-container"]',
			canvas: '[name="dyily-trading-container"] canvas',
			settingCurrency: '[name="widget-menu"]',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]'
		},
		marketDepthWidget: {
			container: '[name="market-depth-container"]',
			settingExchange: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			settingCurrency: '[name="settings-wrapper"] [name="settings-item"]:nth-child(2)',
			settingDepth: '[name="settings-wrapper"] [name="settings-item"]:nth-child(3)'
		},
		quotesWidget: {
			container: '[name="quotes-container"]',
			settingExchange: '[name="widget-menu"]',
			settingCurrencies: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)'
		},
		liquidityWidget: {
			container: '[name="liquidity-container"]',
			settingCurrency: '[name="widget-menu"] [name="custom-select"]:nth-child(1)',
			settingBidAsk: '[name="widget-menu"] [name="custom-select"]:nth-child(2)',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)'
		},
		arbitrageWidget: {
			container: '[name="arbitrage-container"]',
			settingCurrencies: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(2)',
			settingIncome: '[name="settings-wrapper"] [name="settings-item"]:nth-child(3)'
		}
	},
	blockchainBalanceDashboard: {
		BBhotWidget: {
			container: '[name="bb-hot-container"]',
			settingExchanges: '[name="settings-item"]'
		},
		BBcoldWidget: {
			container: '[name="bb-cold-container"]',
			settingExchanges: '[name="settings-item"]'
		},
		walletDetailsWidget: {
			container: '[name="bb-detail-container"]',
			selectedExchangeNameInHeader: 'span:nth-child(3)'
		},
		flowDynamicsWidget: {
			container: '[name="flow-dynamics-container"]'
		}
	},
	arbitrageDashboard: {
		listWidget: {
			container: '[name="arbitrage-full-container"]',
			settingCurrencies: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(2)',
			settingIncome: '[name="settings-wrapper"] [name="settings-item"]:nth-child(3)'
		},
		matrixWidget: {
			container: '[name="arbitrage-matrix-container"]',
			settingCurrency: '[name="widget-menu"] [name="custom-select"]:nth-child(1)',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			settingIncome: '[name="settings-wrapper"] [name="settings-item"]:nth-child(2)',
			notEmptyCell: '[name="widget-cell"] p'

		},
		listDetailWidget: {
			container: '[name="list-details-container"]',
			elementWithTextContent: 'svg'
		},
		matrixDetailWidget: {
			container: '[name="matrix-details-container"]',
			elementWithTextContent: 'svg'
		}
	},
	tradingDashboard: {
		chartWidget: {
			container: '[name="trading-container"]',
		},
		listWidget: {
			container: '[name="list-view-container"]',
			settingExchanges: '[name="settings-wrapper"] [name="settings-item"]:nth-child(1)',
			settingCurrencies: '[name="settings-wrapper"] [name="settings-item"]:nth-child(2)'
		},
		marketDepthWidget: {
			container: '[name="market-depth-hr-container"]',
			settingDepth: '[name="settings-wrapper"] [name="settings-item"]',
			currencyPairInHeader: '[name="depth-select-item"]:nth-child(1)',
			exchangeInHeader: '[name="depth-select-item"]:nth-child(2)'
		}
	},
	widgetCommon: {
		descriptionIcon:'[name="description-icon"]',
		settingsIcon: '[name="settings-icon"]',
		settingsWrapper: '[name="settings-wrapper"]',
		settingSingleSelect: {
			inputArrow: 'span',
			firstDropdownItem: '[name="dropdown"] ul>li:nth-child(1)',
			secondDropdownItem: '[name="dropdown"] ul>li:nth-child(2)',
			thirdDropdownItem: '[name="dropdown"] ul>li:nth-child(3)'
		},
		settingMultiselect: {
			firstDropdownItem: '[name="dropdown"] ul>li:nth-child(1)',
			inputArrow: '[name="input-arrow"]',
		},
		settingsDropdown: '[name="dropdown"]',
		widgetList: '[name="widget-list"]',
		listItem: '[name="widget-list-item"]',
		secondListItem: '[name="widget-list-item"]:nth-child(2)',
		fullscreenIcon: '[name="screen-icon"]',
		cell: '[name="widget-cell"]',
		textInput: '[name="input-wrapper"]',
		head: '[name="widget-list-head"]'
	},
	description: {
		wrapper: '[name="description-wrapper"]',
		scoringDashboard: {
			ratingWidgetTab: '[data-tab-name="rating"]',
			scoringWidgetTab: '[data-tab-name="scoring"]',
			exchangeProfileWidgetTab: '[data-tab-name="exchange-profile"]',
			modelsAssumptionTab: '[data-tab-name="assumpsions"]'
		},
		liquidityDashboard: {
			currencyWidgetTab: '[data-tab-name="currency"]',
			quotesWidgetTab: '[data-tab-name="quotes"]',
			dailyTradingWidgetTab: '[data-tab-name="daily-trading"]',
			liquidityWidgetTab: '[data-tab-name="liquidity"]',
			marketDepthWidgetTab: '[data-tab-name="market-depth"]',
			arbitrageWidgetTab: '[data-tab-name="arbitrage"]'
		},
		blockchainBalanceDashboard: {
			BBHotWalletsWidgetTab: '[data-tab-name="bb-hot"]',
			BBColdWalletsWidgetTab: '[data-tab-name="bb-cold"]',
			walletDetailsWidgetTab: '[data-tab-name="wallet-details"]',
			flowDynamicsWidgetTab: '[data-tab-name="flow-dynamics"]'
		},
		arbitrageDashboard: {
			arbitrageWidgetTab: '[data-tab-name="arbitrage-list"]',
			matrixWidgetTab: '[data-tab-name="arbitrage-matrix"]',
			listDetailWidgetTab: '[data-tab-name="arbitrage-list-detail"]',
			matrixDetailWidgetTab: '[data-tab-name="arbitrage-matrix-detail"]'
		},
		tradingDashboard: {
			chartWidgetTab:'[data-tab-name="trading-view"]',
			listWidgetTab: '[data-tab-name="list-view"]',
			marketDepthWidgetTab: '[data-tab-name="market-depth"]'
		}
	},
	// MOBILE
	mobile: {
		burgerMenuButton: '[name="burger-menu"]',
		menu: {
			rating: '[name="menu-item-rating"]',
			currency: '[name="menu-item-currency"]',
			quotes: '[name="menu-item-quotes"]',
			liquidity: '[name="menu-item-liquidity"]',
			bb: '[name="menu-item-bb"]',
			arbitrage: '[name="menu-item-arbitrage"]',
			dailyTrading: '[name="menu-item-daily-trading"]',
			subscribeButton: '[name="subscribe-button"]',
			socialIconsContainer: '[name="social-icons"]',
		},
		ratingWidget: {
			container: '[name="rating-container"]',
			sortingItem: {
				rating: '[name="rating"]',
				exchange: '[name="exchange"]',
				general: '[name="general"]',
				liquidity: '[name="liquidity"]',
				cyberSecurity: '[name="cyber-security"]',
				publicOpinion: '[name="public-opinion"]',
				withdrawalAndLimits: '[name="withdrawal-and-limits"]'
			}
		},
		currencyWidget: {
			container: '[name="currency-container"]',
			sortingItem: {
				exchanges: '[name="exchanges"]',
				lastPrice: '[name="last-price"]',
				bid: '[name="bid"]',
				ask: '[name="ask"]'
			}
		},
		quotesWidget: {
			container: '[name="quotes-container"]',
			sortingItem: {
				currency: '[name="currency"]',
				lastPrice: '[name="last-price"]',
				bid: '[name="bid"]',
				ask: '[name="ask"]'
			}
		},
		liquidityWidget: {
			container: '[name="liquidity-container"]',
			sortingItem: {
				exchanges: '[name="exchanges"]',
				percent1: '[name="percent-1"]',
				percent5: '[name="percent-5"]',
				percent10: '[name="percent-10"]',
				percent25: '[name="percent-25"]'
			}
		},
		bbWidget: {
			container: '[name="bb-container"]',
			sortingItem: {
				exchange: '[name="exchange"]',
				hot: '[name="hot"]',
				cold: '[name="cold"]',
				balance: '[name="balance"]',
				inflowVol: '[name="inflow-vol"]',
				outflowVol: '[name="outflow-vol"]',
				inflowTrans: '[name="inflow-trans"]',
				outflowTrans: '[name="inflow-trans"]'
			}
		},
		arbitrageWidget: {
			container: '[name="arbitrage-container"]',
			sortingItem: {
				currency: '[name="currency"]',
				exchanges: '[name="exchanges"]',
				income: '[name="income"]',
				volume: '[name="volume"]'
			}
		},
		dailyTradingWidget: {
			container: '[name="daily-trading-wrapper"]'
		},
		widgetCommon: {
			head: '[name="widget-head"]',
			headSorting: '[name="widget-list-head"]',
			descriptionIcon: '[name="description"]',
			settingsIcon: '[name="settings"]',
			settingsOKbutton: '[name="OKbutton"]'
		}
	}, // MOBILE - end
	socialIcons: {
		telegram: '[name="social-icon-telegram"]',
		facebook: '[name="social-icon-facebook"]',
		reddit: '[name="social-icon-reddit"]',
		twitter: '[name="social-icon-twitter"]'
	},
	redirectURLs: {
		telegram: 'https://t.me/hacken_en',
		facebook: 'https://www.facebook.com/hacken.io/',
		reddit: 'https://www.reddit.com/r/hacken/',
		twitter: 'https://twitter.com/Hacken_io',
		blog: 'https://blog.cryptoexchangeranks.com',
		cerLanding: 'https://cryptoexchangeranks.com'
	},
	emailSubscriptionPopup: {
		container: '.mc-modal',
		emailInput: '#mc-EMAIL',
		nameInput: '#mc-FNAME',
		countryDropdown: '#mc-COUNTRY',
		userTypeDropdown: '#mc-USER_TYPE',
		checkbox: 'input[type="checkbox"]',
		submitButton: 'input[value="SUBSCRIBE"]',
		success: '.popup-signup-success'
	}
}
