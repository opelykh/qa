import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

jest.setTimeout(10000);
describe("BB hot wallets view widget", () => {
  let widgetContainer = locators.blockchainBalanceDashboard.BBhotWidget.container;
  let settingExchanges = locators.blockchainBalanceDashboard.BBhotWidget.settingExchanges;

  it("Exchanges multiselect", async () => {
    await page.waitForSelector(locators.widgetCommon.listItem);
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count items after deselecting exchanges
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling).toBeLessThan(amountOfAllItems);
  });
});

describe("BB cold wallets view widget", () => {
  let widgetContainer = locators.blockchainBalanceDashboard.BBcoldWidget.container;
  let settingExchanges = locators.blockchainBalanceDashboard.BBcoldWidget.settingExchanges;

  it("Exchanges multiselect", async () => {
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count items after deselecting exchanges
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling).toBeLessThan(amountOfAllItems);
  });
});
