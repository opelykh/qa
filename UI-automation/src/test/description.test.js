import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js';
import helpers from '../helpers.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
  await page.goto(url);
  await page.waitForSelector(locators.header.container), { timeout: 10000 };
})

afterAll(() => {
  browser.close();
});

describe("Description", () => {
  it("Description of each dashboard could be opened", async () => {
    await page.waitForSelector('#header');
    // open description, check presense of description container
    //scoring Dashboard
    await MainPage.openDashboardDescription(page, locators.header.dashboardTabs.scoring);
    await MainPage.switchToDescriptionTab(page, locators.description.scoringDashboard.ratingWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.scoringDashboard.scoringWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.scoringDashboard.exchangeProfileWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.scoringDashboard.modelsAssumptionTab);
    await MainPage.closeDescription(page);
    // liquidity Dashboard
    await MainPage.openDashboard(page, locators.header.dashboardTabs.liquidity);
    await MainPage.openDashboardDescription(page, locators.header.dashboardTabs.liquidity);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.currencyWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.quotesWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.dailyTradingWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.liquidityWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.marketDepthWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.liquidityDashboard.arbitrageWidgetTab);
    await MainPage.closeDescription(page);
    // blockchain Balance Dashboard
    await MainPage.openDashboard(page, locators.header.dashboardTabs.blockchainBalance);
    await MainPage.openDashboardDescription(page, locators.header.dashboardTabs.blockchainBalance);
    await MainPage.switchToDescriptionTab(page, locators.description.blockchainBalanceDashboard.BBHotWalletsWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.blockchainBalanceDashboard.BBColdWalletsWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.blockchainBalanceDashboard.walletDetailsWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.blockchainBalanceDashboard.flowDynamicsWidgetTab);
    await MainPage.closeDescription(page);
    //arbitrage Dashboard
    await MainPage.openDashboard(page, locators.header.dashboardTabs.arbitrage);
    await MainPage.openDashboardDescription(page, locators.header.dashboardTabs.arbitrage);
    await MainPage.switchToDescriptionTab(page, locators.description.arbitrageDashboard.arbitrageWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.arbitrageDashboard.matrixWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.arbitrageDashboard.listDetailWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.arbitrageDashboard.matrixDetailWidgetTab);
    await MainPage.closeDescription(page);
    //trading Dashboard
    await MainPage.openDashboard(page, locators.header.dashboardTabs.trading);
    await MainPage.openDashboardDescription(page, locators.header.dashboardTabs.trading);
    await MainPage.switchToDescriptionTab(page, locators.description.tradingDashboard.chartWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.tradingDashboard.listWidgetTab);
    await MainPage.switchToDescriptionTab(page, locators.description.tradingDashboard.marketDepthWidgetTab);
    await MainPage.closeDescription(page);
  });

  it("Description of each widget could be opened", async () => {
    await page.waitForSelector('#header');
    // scoring
    await Widget.openWidgetDescription(page, locators.scoringDashboard.ratingWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.scoringDashboard.scoringWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.scoringDashboard.exchangeProfileWidget.container);
    await MainPage.closeDescription(page);
    // liquidity
    await MainPage.openDashboard(page, locators.header.dashboardTabs.liquidity);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.currencyWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.quotesWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.dailyTradingWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.liquidityWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.marketDepthWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.liquidityDashboard.arbitrageWidget.container);
    await MainPage.closeDescription(page);
    // blockchain balance
    await MainPage.openDashboard(page, locators.header.dashboardTabs.blockchainBalance);
    await Widget.openWidgetDescription(page, locators.blockchainBalanceDashboard.BBhotWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.blockchainBalanceDashboard.BBcoldWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.blockchainBalanceDashboard.walletDetailsWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.blockchainBalanceDashboard.flowDynamicsWidget.container);
    await MainPage.closeDescription(page);
    // arbitrage
    await MainPage.openDashboard(page, locators.header.dashboardTabs.arbitrage);
    await Widget.openWidgetDescription(page, locators.arbitrageDashboard.listWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.arbitrageDashboard.matrixWidget.container);
    await MainPage.closeDescription(page);
    // trading
    await MainPage.openDashboard(page, locators.header.dashboardTabs.trading);
    await Widget.openWidgetDescription(page, locators.tradingDashboard.listWidget.container);
    await MainPage.closeDescription(page);
    await Widget.openWidgetDescription(page, locators.tradingDashboard.marketDepthWidget.container);
    await MainPage.closeDescription(page);
  });

}, 30000);
