import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'
import EmailSubscriptionPopup from '../pages/EmailSubscriptionPopup.js'
import helpers from '../helpers.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
  await page.goto(url);
  await page.waitForSelector(locators.header.container), { timeout: 10000 };
})

afterAll(() => {
  browser.close();
});

describe("Subscribe", () => {
let pages = null;
let newPage = null;
let url = null;

  it("Subscription", async () => {
    await page.click(locators.footer.subscribeButton);
    await page.waitForSelector(locators.emailSubscriptionPopup.container), { timeout: 5000 };
    const iframe = page.frames()[2];

    await EmailSubscriptionPopup.fillEmail(iframe);
    await EmailSubscriptionPopup.fillName(iframe);
    await EmailSubscriptionPopup.fillCountry(iframe);
    await EmailSubscriptionPopup.fillUserType(iframe);
    await EmailSubscriptionPopup.enableCheckbox(iframe);
    await EmailSubscriptionPopup.submitForm(iframe);
    await iframe.waitForSelector(locators.emailSubscriptionPopup.success);
  });

  it("Social icons", async () => {
      // telegram
      await MainPage.openExternalURL(browser, page, locators.socialIcons.telegram, locators.redirectURLs.telegram, 2000);
      // facebook
      await MainPage.openExternalURL(browser, page, locators.socialIcons.facebook, locators.redirectURLs.facebook, 3000);
      // reddit
      await MainPage.openExternalURL(browser, page, locators.socialIcons.reddit, locators.redirectURLs.reddit, 3000);
      // twitter
      await MainPage.openExternalURL(browser, page, locators.socialIcons.twitter, locators.redirectURLs.twitter, 2000);
  }, 20000);

  it("Blog", async () => {
    await page.click(locators.footer.blogButton);
    await helpers.wait(2000);
    pages = await browser.pages();
    newPage = pages[2];
    url = await newPage.evaluate(() => location.origin);
    await expect(url).toBe(locators.redirectURLs.blog);
    await newPage.close();
  });

  it("Policies popup", async () => {
    await page.click(locators.footer.policies);
    let appropriateTextIsFound =  await page.evaluate(() => document.querySelector('.content-wrapper').innerText.indexOf('Privacy Policy') !== -1);
    await expect(appropriateTextIsFound).toBeTruthy();
  });

}, 30000);
