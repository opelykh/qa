import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js';
import helpers from '../helpers.js';
import MainPageMobile from '../pages/MainPageMobile.js';

const devices = require('puppeteer/DeviceDescriptors');
let browser;
let page;
let url = config.URL;
const phone = devices[config.device];
const phoneLandscape = devices[`${config.device} landscape`];

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await browser.newPage();
  await page.emulate(phone);
  await page.goto(url);
});

afterAll(() => {
  browser.close();
});

describe("Navigation", () => {
  it("Open each page from menu", async () => {
    await MainPageMobile.openRatingWidget(page);
    await MainPageMobile.openCurrencyWidget(page);
    await MainPageMobile.openQuotesWidget(page);
    await MainPageMobile.openLiquidityWidget(page);
    await MainPageMobile.openBBwidget(page);
    await MainPageMobile.openArbitrageWidget(page);
    await MainPageMobile.openDailyTradingWidget(page);
  }, 10000);

}, 20000);
