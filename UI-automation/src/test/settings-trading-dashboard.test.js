import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

describe("List view widget", () => {
  let widgetContainer = locators.tradingDashboard.listWidget.container;
  let settingExchanges = locators.tradingDashboard.listWidget.settingExchanges;
  let settingCurrencies = locators.tradingDashboard.listWidget.settingCurrencies;

  it("Exchanges", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // disable exchange items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Currencies ", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // disable currency items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingCurrencies, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Fullscreen", async () => {
    await Widget.enableDisableFullscreen(page, widgetContainer);
    let isWidgetFullscreen = await Widget.checkIfWidgetIsInFullscreen(page, widgetContainer);
    await expect(isWidgetFullscreen).toBeTruthy();
    await Widget.enableDisableFullscreen(page, widgetContainer);
  });

});

describe("Market depth view widget", () => {
  let widgetContainer = locators.tradingDashboard.marketDepthWidget.container;
  let settingDepth = locators.tradingDashboard.marketDepthWidget.settingDepth;

  it("Depth singleselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // change depth
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingDepth);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
  });

});
