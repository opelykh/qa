import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

const devices = require('puppeteer/DeviceDescriptors');
const iPhone = devices['iPhone 4'];
const headless = config.headless;
let url = config.URL;

let page;
let browser;

beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: headless,
    slowMo: 200,
    devtools: false
  });
  page = await browser.newPage();

  await page.emulate(devices['iPhone 8']);
  // await page.setViewport({isLandscape: true});
});

afterAll(() => {
  browser.close();
});

describe("CER main page", () => {

  it("Navigation between tabs", async () => {
    jest.setTimeout(15000);
    function wait (ms) {
      return new Promise(resolve => setTimeout(() => resolve(), ms));
    }
    await page.goto(url);
    await page.waitForSelector('#root');
    await wait(500);
    await page.click('div#root>div>div>div>div');
    await wait (500);
    await page.waitForSelector('a[href="#/mobile/currency"]');
    await page.emulate(devices['iPhone 8 landscape']);
    await page.evaluate(() => {
      document.querySelector('a[href="#/mobile/policies"').scrollIntoView({
      behavior: 'smooth'
      });
    });
    // await wait(1000);

    await page.click('a[href="#/mobile/policies"]');
    await page.emulate(devices['iPhone 8']);
    // await wait(1000);

    await page.evaluate(() => {
      document.querySelector('a[href="https://youradchoices.com/"]').scrollIntoView({
      behavior: 'smooth'
      });
    });
    await wait(2000);
    // await page.screenshot({path: './screenshots/landscape.png'});

    await page.click('div#root>div>div>div>div');
    await page.waitForSelector('a[href="#/mobile/currency"]');
    await page.click('a[href="#/mobile/currency"]')

    await page.click('div#root>div>div>div>div');
    await page.waitForSelector('a[href="#/mobile/quotes"]');
    await page.click('a[href="#/mobile/quotes"]')

    await page.click('div#root>div>div>div>div');
    await page.click('a[href="#/mobile/blockchain-balance"]')

    await page.click('div#root>div>div>div>div');
    await page.click('a[href="#/mobile/arbitrage"]')
    // await page.emulate(devices['iPhone 8 landscape']);

    await wait(500);
    const title = await page.title();
    expect(title).toBe("CER");

    await page.evaluate(
      () => { let d = document.createElement('div');
              document.body.innerHTML = '';
              d.style.paddingTop = '250px';
              document.body.style.fontSize = '2em';
              d.innerText = 'Amazing framework :)';
              document.body.appendChild(d);
              document.body.style.color = '#00e75f';
              document.body.style.textAlign = 'center';
      }
    );
    await wait(3000);
  }, 25000);

});
