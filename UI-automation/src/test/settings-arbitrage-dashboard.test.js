import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

describe("List view widget", () => {
  let widgetContainer = locators.arbitrageDashboard.listWidget.container;
  let settingCurrencies = locators.arbitrageDashboard.listWidget.settingCurrencies;
  let settingExchanges = locators.arbitrageDashboard.listWidget.settingExchanges;
  let settingIncome = locators.arbitrageDashboard.listWidget.settingIncome;

  it("Currencies", async () => {
    // wait for settings will be applied to widget
    await wait(1000);
    await page.waitForSelector(locators.widgetCommon.listItem);
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // disable currency items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingCurrencies, 10);
    // wait for settings will be applied to widget
    await wait(1000);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Exchanges", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // disable exchange items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Income", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    await Widget.typeValueToInput(page, widgetContainer, settingIncome, '.1')
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

});

describe("Matrix view widget", () => {
  let widgetContainer = locators.arbitrageDashboard.matrixWidget.container;
  let settingCurrency = locators.arbitrageDashboard.matrixWidget.settingCurrency;
  let settingExchanges = locators.arbitrageDashboard.matrixWidget.settingExchanges;
  let settingIncome = locators.arbitrageDashboard.matrixWidget.settingIncome;

  it("Exchanges multiselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // await console.log(amountOfAllItems);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 2);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // wait for settings will be applied to widget
    await wait(1000);
    // count items after deselecting exchanges
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // await console.log(amountOfItemsAfterDisabling);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling).toBeLessThan(amountOfAllItems);
  });

  it("Currency pair singleselect", async () => {
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // select 2nd dropdown item
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingCurrency);
    // wait for settings will be applied to widget
    await wait(1000);
    // count items after selecting another pair
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling).toBeLessThan(amountOfAllItems);

    // select first currency item (post condition)
    await Widget.selectFirstSingleSelectItem(page, widgetContainer, settingCurrency);
  });


  it("Income", async () => {
    // wait for settings will be applied to widget
    await wait(2000);
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count all empty cells
    let emptyCellsAmount = await Widget.countEmptyCells(page, widgetContainer);
    // clear input
    await Widget.clearInput(page, widgetContainer, settingIncome);
    await Widget.typeValueToInput(page, widgetContainer, settingIncome, '1')
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // wait for settings will be applied to widget
    await wait(2000);
    // // count empty cells again
    let emptyCellsAfterChange = await Widget.countEmptyCells(page, widgetContainer);
    // compare results
    expect(emptyCellsAfterChange).toBeGreaterThan(emptyCellsAmount);
  });

});
