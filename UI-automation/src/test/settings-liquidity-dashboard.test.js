import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

describe("Currency view widget", () => {
  let widgetContainer = locators.liquidityDashboard.currencyWidget.container;
  let settingCurrency = locators.liquidityDashboard.currencyWidget.settingCurrency;
  let settingExchanges = locators.liquidityDashboard.currencyWidget.settingExchanges;

  it("Currency pair singleselect", async () => {
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // select 2nd dropdown item
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingCurrency);
    // count items after selecting another pair
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling == amountOfAllItems).toBeFalsy();
  });

  it("Exchanges multiselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 2);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count items after deselecting exchanges
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of exchanges is changed
    await expect(amountOfItemsAfterDisabling).toBe(amountOfAllItems - 2);
  });

  it("Fullscreen", async () => {
    await Widget.enableDisableFullscreen(page, widgetContainer);
    let isWidgetFullscreen = await Widget.checkIfWidgetIsInFullscreen(page, widgetContainer);
    await expect(isWidgetFullscreen).toBeTruthy();
    await Widget.enableDisableFullscreen(page, widgetContainer);
  });

});

describe("Quotes view widget", () => {
  let widgetContainer = locators.liquidityDashboard.quotesWidget.container;
  let settingExchange = locators.liquidityDashboard.quotesWidget.settingExchange;
  let settingCurrencies = locators.liquidityDashboard.quotesWidget.settingCurrencies;

  it("Exchange singleselect", async () => {
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // select 2nd dropdown item
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingExchange);
    // count items after selecting another pair
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of currency pairs is changed
    await expect(amountOfItemsAfterDisabling).toBeLessThan(amountOfAllItems);
  });

  it("Currency multiselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingCurrencies, 2);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count items after deselecting currency pairs
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // check that amount of currency pairs is changed
    await expect(amountOfItemsAfterDisabling).toBe(amountOfAllItems - 2);
  });

  it("Fullscreen", async () => {
    await Widget.enableDisableFullscreen(page, widgetContainer);
    let isWidgetFullscreen = await Widget.checkIfWidgetIsInFullscreen(page, widgetContainer);
    await expect(isWidgetFullscreen).toBeTruthy();
    await Widget.enableDisableFullscreen(page, widgetContainer);
  });

});

describe("Daily trading view widget", () => {
  let widgetContainer = locators.liquidityDashboard.dailyTradingWidget.container;
  let settingCurrency = locators.liquidityDashboard.dailyTradingWidget.settingCurrency;
  let settingExchanges = locators.liquidityDashboard.dailyTradingWidget.settingExchanges;

  it("Currency pair singleselect", async () => {
    // count availabe exchanges list
    await Widget.openCloseSettings(page, widgetContainer);
    let exchangesList = await Widget.countDropdownItemsInMultiselectDropdown(page, widgetContainer, settingExchanges);
    // change currency dropdown item
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingCurrency);
    // canvas is still be present
    await expect(await page.$(locators.liquidityDashboard.dailyTradingWidget.canvas)).toBeTruthy();
    // count available exchanges list after change currency pairs
    let newExchangesList = await Widget.countDropdownItemsInMultiselectDropdown(page, widgetContainer, settingExchanges);
    await Widget.openCloseSettings(page, widgetContainer);
    await expect(newExchangesList).toBeLessThan(exchangesList);
  });

  it("Fullscreen", async () => {
    let canwasSize =  await Widget.measureCanvasSize(page, widgetContainer);
    await Widget.enableDisableFullscreen(page, widgetContainer);
    let canwasSizeInFullscreen =  await Widget.measureCanvasSize(page, widgetContainer);
    await expect(canwasSizeInFullscreen).toBeGreaterThan(canwasSize);
    await Widget.enableDisableFullscreen(page, widgetContainer);
  });

  it("Exchanges multiselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // disable dropdown items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // check canvas presence
    let canvasIsPresent = await page.$(locators.liquidityDashboard.dailyTradingWidget.canvas);
    await expect(canvasIsPresent).toBeFalsy();
  });

});

describe("Liquidity view widget", () => {
  let widgetContainer = locators.liquidityDashboard.liquidityWidget.container;
  let settingCurrency = locators.liquidityDashboard.liquidityWidget.settingCurrency;
  let settingBidAsk = locators.liquidityDashboard.liquidityWidget.settingBidAsk;
  let settingExchanges = locators.liquidityDashboard.liquidityWidget.settingExchanges;

    it("Currency singleselect", async () => {
      // count widget list items
      let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
      // select 2nd dropdown item
      await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingCurrency);
      // count items after selecting another pair
      let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
      // check that amount of currency pairs is changed
      await expect(amountOfItemsAfterDisabling).not.toBe(amountOfAllItems);
    });

    it("Exchanges multiselect", async () => {
      // open settings
      await Widget.openCloseSettings(page, widgetContainer);
      // count widget list items
      let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
      // disable dropdown items
      await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 2);
      // close settings
      await Widget.openCloseSettings(page, widgetContainer);
      // count items after deselecting currency pairs
      let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
      // check that amount of currency pairs is changed
      await expect(amountOfItemsAfterDisabling).toBe(amountOfAllItems - 2);
    });

    it("Fullscreen", async () => {
      await Widget.enableDisableFullscreen(page, widgetContainer);
      let isWidgetFullscreen = await Widget.checkIfWidgetIsInFullscreen(page, widgetContainer);
      await expect(isWidgetFullscreen).toBeTruthy();
      await Widget.enableDisableFullscreen(page, widgetContainer);
    });

    it("Bid/Ask singleselect", async () => {
      // save first cell content
      let firstCellPrice = await Widget.getFirstCellPrice(page, widgetContainer);
      // change bid to ask
      await Widget.changeBidAskSingleSelectItem(page, widgetContainer, settingBidAsk);
      // check first cell content
      let cellContentAfterChange = await Widget.getFirstCellPrice(page, widgetContainer);
      // compare contents before and after change
      await expect(cellContentAfterChange).not.toBe(firstCellPrice);
    });
});

describe("Market depth view widget", () => {
  let widgetContainer = locators.liquidityDashboard.marketDepthWidget.container;
  let settingExchange = locators.liquidityDashboard.marketDepthWidget.settingExchange;
  let settingCurrency = locators.liquidityDashboard.marketDepthWidget.settingCurrency;
  let settingDepth = locators.liquidityDashboard.marketDepthWidget.settingDepth;

  it("Exchange singleselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // save widget price data
    let priceBeforeChange = Widget.getAvgBidPrice(page, widgetContainer);
    // count availabe currency pairs in setting dropdown
    let currencyItems = await Widget.countDropdownItems(page, widgetContainer, settingCurrency, 'single');
    // change exchange
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer, settingExchange);
    // count available currency pairs in setting dropdown
    let currencyItemsAfterChange = await Widget.countDropdownItems(page, widgetContainer, settingCurrency, 'single');
    // compare currency pairs amounts
    await expect(currencyItemsAfterChange).not.toEqual(currencyItems);
    // save widget price data
    let priceAfterChange = await Widget.getAvgBidPrice(page, widgetContainer);
    // compare prices before and after change
    await expect(priceAfterChange).not.toEqual(priceBeforeChange);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
  });

  it("Currency singleselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // save widget price data
    let priceBeforeChange = await Widget.getAvgBidPrice(page, widgetContainer);
    // change currency
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer,
      locators.liquidityDashboard.marketDepthWidget.settingCurrency);
    // save widget price data
    let priceAfterChange = await Widget.getAvgBidPrice(page, widgetContainer);
    // compare prices before and after change
    await expect(priceAfterChange).not.toEqual(priceBeforeChange);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
  });

  it("Depth singleselect", async () => {
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // save widget price data
    let priceBeforeChange = await Widget.getAvgBidPrice(page, widgetContainer);
    // change depth
    await Widget.choose3rdItemInSingleSelect(page, widgetContainer,
      locators.liquidityDashboard.marketDepthWidget.settingDepth);
    // save widget price data
    let priceAfterChange = await Widget.getAvgBidPrice(page, widgetContainer);
    // compare prices before and after change
    await expect(priceAfterChange).not.toEqual(priceBeforeChange);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
  });

});

describe("Arbitrage view widget", () => {
  let widgetContainer = locators.liquidityDashboard.arbitrageWidget.container;
  let settingCurrencies = locators.liquidityDashboard.arbitrageWidget.settingCurrencies;
  let settingExchanges = locators.liquidityDashboard.arbitrageWidget.settingExchanges;
  let settingIncome = locators.liquidityDashboard.arbitrageWidget.settingIncome;

  it("Currencies multiselect", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // count exchanges dropdown currencyItems
    let amountOfItemsInExchgeDropdown = await Widget.countDropdownItems(page, widgetContainer, settingExchanges, 'multiselect');
    // disable currency items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingCurrencies, 10);
      // count exchanges dropdown currencyItems
      let amountOfItemsInExchgeDropdownAfterChange = await Widget.countDropdownItems(page, widgetContainer, settingExchanges, 'multiselect');
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
    await expect(amountOfItemsInExchgeDropdownAfterChange).toBeLessThan(amountOfItemsInExchgeDropdown);
  });

  it("Exchange multiselect", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    // disable exchange items
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Currency income", async () => {
    // count widget list items
    let amountOfAllWidgetListItems = await Widget.countWidgetListItems(page, widgetContainer);
    await Widget.openCloseSettings(page, widgetContainer);
    await Widget.typeValueToInput(page, widgetContainer, settingIncome, '.1')
    // close settings
    await Widget.openCloseSettings(page, widgetContainer);
    // count widget list items
    let amountOfWidgetListItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    // compare results
    await expect(amountOfWidgetListItemsAfterDisabling).toBeLessThan(amountOfAllWidgetListItems);
  });

  it("Fullscreen", async () => {
    await Widget.enableDisableFullscreen(page, widgetContainer);
    let isWidgetFullscreen = Widget.checkIfWidgetIsInFullscreen(page, widgetContainer);
    await expect(isWidgetFullscreen).toBeTruthy();
    await Widget.enableDisableFullscreen(page, widgetContainer);
  });

});
