import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
  await page.goto(url);
  await page.waitForSelector(locators.header.container);
})

afterAll(() => {
  browser.close();
});

describe("Navigation", () => {

  it('Title should be CER', async() => {
    const title = await page.title();
    await expect(title).toBe("CER");
  });

  it("Navigation between tabs; selected tab becomes active", async () => {
    await MainPage.openDashboard(page, locators.header.dashboardTabs.liquidity);
    let activeTabAttribute = await page.$eval(locators.header.dashboardTabs.liquidity, e => e.getAttribute('aria-current'));
    await expect(activeTabAttribute).toBe('page');

    await MainPage.openDashboard(page, locators.header.dashboardTabs.blockchainBalance);
    activeTabAttribute = await page.$eval(locators.header.dashboardTabs.blockchainBalance, e => e.getAttribute('aria-current'));
    await expect(activeTabAttribute).toBe('page');

    await MainPage.openDashboard(page, locators.header.dashboardTabs.arbitrage);
    activeTabAttribute = await page.$eval(locators.header.dashboardTabs.arbitrage, e => e.getAttribute('aria-current'));
    await expect(activeTabAttribute).toBe('page');

    await MainPage.openDashboard(page, locators.header.dashboardTabs.trading);
    activeTabAttribute = await page.$eval(locators.header.dashboardTabs.trading, e => e.getAttribute('aria-current'));
    await expect(activeTabAttribute).toBe('page');

    await MainPage.openDashboard(page, locators.header.dashboardTabs.scoring);
    activeTabAttribute = await page.$eval(locators.header.dashboardTabs.scoring, e => e.getAttribute('aria-current'));
    await expect(activeTabAttribute).toBe('page');
  });

  it("Check all widgets presense on each dashboard", async () => {
    let liquidityDashboard = await MainPage.openLiquidityDashboard(page);
    await liquidityDashboard.checkPresenceOfWidgets(page);

    let blockchainBalanceDashboard = await MainPage.openBlockchainBalanceDashboard(page);
    await blockchainBalanceDashboard.checkPresenceOfWidgets(page);

    let arbitrageDashboard = await MainPage.openArbitrageDashboard(page);
    await arbitrageDashboard.checkPresenceOfWidgets(page);

    let tradingDashboard = await MainPage.openTradingDashboard(page);
    await tradingDashboard.checkPresenceOfWidgets(page);

    let scoringDashboard = await MainPage.openScoringDashboard(page);
    await scoringDashboard.checkPresenceOfWidgets(page);
  });

});
