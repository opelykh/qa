import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js';
import helpers from '../helpers.js';
import ScoringDashboard from '../pages/ScoringDashboard.js';
import BlockchainBalanceDashboard from '../pages/BlockchainBalanceDashboard.js';
import ArbitrageDashboard from '../pages/ArbitrageDashboard.js';
import TradingDashboard from '../pages/TradingDashboard.js';

let ScoringD = new ScoringDashboard();
let BlockchainBalanceD = new BlockchainBalanceDashboard();
let ArbitrageD = new ArbitrageDashboard();
let TradingD = new TradingDashboard();

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;

let browser;
let page;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {})

afterAll(() => {
  browser.close();
});

describe("Scoring dashboard", () => {
  let ratingWidgetContainer = locators.scoringDashboard.ratingWidget.container;
  it("Data on Exchange profile widget is changed after selecting another item on Rating widget", async () => {
    let exchangeProfileWIdgetData = await ScoringD.getDataFromExchangeProfileWidget(page);
    await Widget.selectWidgetListItem(page, ratingWidgetContainer, 2);
    let exchangeProfileWIdgetDataAfterChange = await ScoringD.getDataFromExchangeProfileWidget(page);
    await expect(exchangeProfileWIdgetDataAfterChange).not.toBe(exchangeProfileWIdgetData);
  });

  it("Detaild are displayed on Scoring widget", async () => {
    await ScoringD.expandListItemAndCheckIfDetailsAppear(page);
  });

});

describe("BB dashboard", () => {
  let bbHotWidgetContainer = locators.blockchainBalanceDashboard.BBcoldWidget.container;
  let bbColdWidgetContainer = locators.blockchainBalanceDashboard.BBhotWidget.container;
  let detailsWidgetContainer = locators.blockchainBalanceDashboard.walletDetailsWidget.container;
  let flowDynamicsWidgetContainer = locators.blockchainBalanceDashboard.flowDynamicsWidget.container;

  it("Wallet details and flow dymanics data are updated after selecting another item on BB hot widget", async () => {
    await MainPage.openBlockchainBalanceDashboard(page);
    let exchangeNameOnDetailsWidget = await BlockchainBalanceD.defineSelectedExchange(page, detailsWidgetContainer);
    let exchangeNameOnFlowDynamicsWidget = await BlockchainBalanceD.defineSelectedExchange(page, flowDynamicsWidgetContainer);
    await Widget.selectWidgetListItem(page, bbHotWidgetContainer, 2);
    let exchangeNameOnDetailsWidgetAfterChange = await BlockchainBalanceD.defineSelectedExchange(page, detailsWidgetContainer);
    let exchangeNameOnFlowDynamicsWidgetAfterChange = await BlockchainBalanceD.defineSelectedExchange(page, flowDynamicsWidgetContainer);
    expect(exchangeNameOnDetailsWidgetAfterChange).not.toBe(exchangeNameOnDetailsWidget);
    expect(exchangeNameOnFlowDynamicsWidgetAfterChange).not.toBe(exchangeNameOnFlowDynamicsWidget);
  });

  it("Wallet details and flow dymanics data are updated after selecting another item on BB cold widget", async () => {
    await MainPage.openBlockchainBalanceDashboard(page);
    let exchangeNameOnDetailsWidget = await BlockchainBalanceD.defineSelectedExchange(page, detailsWidgetContainer);
    let exchangeNameOnFlowDynamicsWidget = await BlockchainBalanceD.defineSelectedExchange(page, flowDynamicsWidgetContainer);
    await Widget.selectWidgetListItem(page, bbColdWidgetContainer, 3);
    let exchangeNameOnDetailsWidgetAfterChange = await BlockchainBalanceD.defineSelectedExchange(page, detailsWidgetContainer);
    let exchangeNameOnFlowDynamicsWidgetAfterChange = await BlockchainBalanceD.defineSelectedExchange(page, flowDynamicsWidgetContainer);
    expect(exchangeNameOnDetailsWidgetAfterChange).not.toBe(exchangeNameOnDetailsWidget);
    expect(exchangeNameOnFlowDynamicsWidgetAfterChange).not.toBe(exchangeNameOnFlowDynamicsWidget);
  });

  it("The same exchange is always selected on bbHot and bbCold widgets", async () => {
    // select 2nd item on bbHot and check selection on bbCold
    // select 1st item (reset selection to default)
    await Widget.selectWidgetListItem(page, bbHotWidgetContainer, 1)
    // select 2nd item on bbHot
    await Widget.selectWidgetListItem(page, bbHotWidgetContainer, 2);
    // check if 2nd item is also selected on bbCold
    let isSecondItemSelectedOnBBcoldWidget = await Widget.checkIfWidgetListItemIsSelected(page, bbColdWidgetContainer, 2);
    await expect(isSecondItemSelectedOnBBcoldWidget).toBeTruthy();

    // select 2nd item on bbCold and check selection on bbHot
    // select 1st item (reset selection to default)
    await Widget.selectWidgetListItem(page, bbColdWidgetContainer, 1)
    // select 2nd item on bbCold
    await Widget.selectWidgetListItem(page, bbColdWidgetContainer, 2);
    // check if 2nd item is also selected on bbHot
    let isSecondItemSelectedOnBBhotWidget = await Widget.checkIfWidgetListItemIsSelected(page, bbHotWidgetContainer, 2);
    await expect(isSecondItemSelectedOnBBhotWidget).toBeTruthy();
  });

});

describe("Arbitrage dashboard", () => {
  let listWidgetContainer = locators.arbitrageDashboard.listWidget.container;
  let matrixWidgetContainer = locators.arbitrageDashboard.matrixWidget.container;
  let listDetailWidgetContainer = locators.arbitrageDashboard.listDetailWidget.container;
  let matrixDetailWidgetContainer = locators.arbitrageDashboard.matrixDetailWidget.container;

  it("Data on List detail widget is changed after selecting another item on List widget", async () => {
    await MainPage.openArbitrageDashboard(page);
    // wait for data loading
    await page.waitForSelector(locators.arbitrageDashboard.matrixWidget.notEmptyCell), {timeout: 5000};
    // save chart text to variable
    let chartTextContent = await ArbitrageD.getTextFromChart(page, listDetailWidgetContainer);
    // select another item on list widget
    await Widget.selectWidgetListItem(page, listWidgetContainer, 2);
    // save chart text to variable
    let chartTextContentAfterChange = await ArbitrageD.getTextFromChart(page, listDetailWidgetContainer);
    expect(chartTextContentAfterChange).not.toBe(chartTextContent);
  });

  it("Data on Matrix detail widget is changed after selecting another item on Matrix widget", async () => {
    await MainPage.openArbitrageDashboard(page);
    // wait for data loading
    await page.waitForSelector(locators.arbitrageDashboard.matrixWidget.notEmptyCell), {timeout: 10000};;
    // save chart text to variable
    let chartTextContent = await ArbitrageD.getTextFromChart(page, matrixDetailWidgetContainer);
    // select another item on list widget
    await ArbitrageD.clickOnNotEmptyCellOnMatrixWidget(page);
    // save chart text to variable
    let chartTextContentAfterChange = await ArbitrageD.getTextFromChart(page, matrixDetailWidgetContainer);
    // click on second widget cell
    if (chartTextContentAfterChange === chartTextContent) {
      await ArbitrageD.clickOnSecondNotEmptyCellOnMatrixWidget(page);
      // save chart text to variable
      let chartTextContentAfterChangeSecondCell = await ArbitrageD.getTextFromChart(page, matrixDetailWidgetContainer);
      await expect(chartTextContentAfterChange === chartTextContent && chartTextContentAfterChangeSecondCell === chartTextContent).not.toBeTruthy();
    } else expect(chartTextContentAfterChange).not.toBe(chartTextContent);
  });

}, 20000);

describe("Trading dashboard", () => {
  let lisContainer = locators.tradingDashboard.listWidget.container;
  let marketDepthContainer = locators.tradingDashboard.marketDepthWidget.container;

  it("Data on market depth widget is changed after selecting another item on List widget", async () => {
    await MainPage.openTradingDashboard(page);
    // wait for data loading
    await page.waitForSelector(`${lisContainer} ${locators.widgetCommon.listItem}`), {
      timeout: 5000
    };;
    await Widget.selectWidgetListItem(page, lisContainer, 1);
    // does not work without whis pause
    await helpers.wait(1000);
    await page.waitForSelector(locators.tradingDashboard.marketDepthWidget.currencyPairInHeader), {
      timeout: 1000
    };
    // save currency pair on Market depth widget to variables
    let currencyPair = await TradingD.marketDepthGetCurrencyPairFromHeader(page);
    let exchange = await TradingD.marketDepthGetExchangeFromHeader(page);
    // select another item on list widget
    await Widget.selectWidgetListItem(page, lisContainer, 2);
    // does not work without this pause
    await helpers.wait(1000);
    await page.waitForSelector(locators.tradingDashboard.marketDepthWidget.currencyPairInHeader), {
      timeout: 1000
    };
    // save currency pair on Market depth widget to variables
    let currencyPairAfterChange = await TradingD.marketDepthGetCurrencyPairFromHeader(page);
    let exchangeAfterChange = await TradingD.marketDepthGetExchangeFromHeader(page);
    await expect(currencyPairAfterChange === currencyPair && exchange === exchangeAfterChange).toBeFalsy();
  }, 15000);

}, 30000);
