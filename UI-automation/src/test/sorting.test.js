import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js';
import helpers from '../helpers.js';

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;

let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

// scoring dashboard
describe("Scoring: Rating view widget", () => {
  let widgetContainer = locators.scoringDashboard.ratingWidget.container;

  it("Exchanges", async () => {
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("General", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Liquidity", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

  it("Cyber security", async () => {
    await Widget.checkSorting(page, widgetContainer, 5);
  });

  it("Public opinion", async () => {
    await Widget.checkSorting(page, widgetContainer, 6);
  });

  it("Withdrawal & limits", async () => {
    await Widget.checkSorting(page, widgetContainer, 7);
  });

});

// liquidity dashboard
describe("Liquidity: Currency view widget", () => {
  let widgetContainer = locators.liquidityDashboard.currencyWidget.container;

  it("Exchanges", async () => {
    await MainPage.openLiquidityDashboard(page);
    await page.waitForSelector(locators.widgetCommon.cell);
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("Bid", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("Ask", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Last price", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

});

describe("Liquidity: Quotes view widget", () => {
  let widgetContainer = locators.liquidityDashboard.quotesWidget.container;

  it("Currency", async () => {
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("Bid", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("Ask", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Last price", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

});

describe("Liquidity: Liquidity view widget", () => {
  let widgetContainer = locators.liquidityDashboard.liquidityWidget.container;

  it("Exchanges", async () => {
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("1%", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("5%", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("10%", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

  it("25%", async () => {
    await Widget.checkSorting(page, widgetContainer, 5);
  });

});

describe("Liquidity: Arbitrage view widget", () => {
  let widgetContainer = locators.liquidityDashboard.arbitrageWidget.container;

  it("Currency", async () => {
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("Exchanges", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("Income", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Volume", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

});

// arbitrage dashboard
describe("Arbitrage: Arbitrage view widget", () => {
  let widgetContainer = locators.arbitrageDashboard.listWidget.container;

  it("Currency", async () => {
    await MainPage.openArbitrageDashboard(page);
    await page.waitForSelector(`${widgetContainer} ${locators.widgetCommon.cell}`);
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("Exchanges", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("Income", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Volume", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

});

// trading dashboard
describe("Trading: List view widget", () => {
  let widgetContainer = locators.tradingDashboard.listWidget.container;

  it("Currency", async () => {
    await MainPage.openTradingDashboard(page);
    await page.waitForSelector(locators.widgetCommon.cell);
    await Widget.checkSorting(page, widgetContainer, 1);
  });

  it("Market", async () => {
    await Widget.checkSorting(page, widgetContainer, 2);
  });

  it("Bid", async () => {
    await Widget.checkSorting(page, widgetContainer, 3);
  });

  it("Ask", async () => {
    await Widget.checkSorting(page, widgetContainer, 4);
  });

  it("Last price", async () => {
    await Widget.checkSorting(page, widgetContainer, 5);
  });

});
