import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'
import EmailSubscriptionPopup from '../pages/EmailSubscriptionPopup.js'
import helpers from '../helpers.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
  await page.goto(url);
  await page.waitForSelector(locators.header.container), { timeout: 10000 };
})

afterAll(() => {
  browser.close();
});


describe("Subscribe", () => {
let pages = null;
let newPage = null;
let url = null;

  it("Logo", async () => {
      // logo
      await page.click(locators.header.logo);
      await helpers.wait(2000);
      pages = await browser.pages();
      newPage = pages[2];
      url = await newPage.evaluate(() => location.origin);
      await expect(url).toBe(locators.redirectURLs.cerLanding);
      await newPage.close();
  });


});
