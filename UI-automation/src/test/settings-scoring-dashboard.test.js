import puppeteer from 'puppeteer';
import locators from '../locators.js';
import config from '../config/config.js'
import MainPage from '../pages/MainPage.js';
import Widget from '../pages/Widget.js'

let url = config.URL;
const width = config.browserWidth;
const height = config.browserHeight;
const headless = config.headless;
let page;
let browser;

beforeAll(async () => {
  browser = await helpers.setupBrowser();
  page = await helpers.setupPage(browser);
  await page.waitForSelector(locators.widgetCommon.listItem);
});

beforeEach(async () => {
})

afterAll(() => {
  browser.close();
});

describe("Rating view widget", () => {
  let widgetContainer = locators.scoringDashboard.ratingWidget.container;
  let settingExchanges = locators.scoringDashboard.ratingWidget.settingExchanges;

  it("Exchanges multiselect", async () => {
    // jest.setTimeout(5000);
    // count widget list items
    let amountOfAllItems = await Widget.countWidgetListItems(page, widgetContainer);
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // disable exchanges in settings
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 5);
    await Widget.openCloseSettings(page, widgetContainer);
    let amountOfItemsAfterDisabling = await Widget.countWidgetListItems(page, widgetContainer);
    await expect(amountOfItemsAfterDisabling).toBe(amountOfAllItems - 5);
  });

});

describe("Scoring view widget", () => {
  let widgetContainer = locators.scoringDashboard.scoringWidget.container;
  let settingExchanges = locators.scoringDashboard.scoringWidget.settingExchanges;

  it("Exchanges multiselect", async () => {
    // wait for widget list item
    await page.waitForSelector(`${widgetContainer} ${locators.scoringDashboard.scoringWidget.listItem}`);
    // count widget list items
    let widgetListItems = await page.$$(`${widgetContainer} ${locators.scoringDashboard.scoringWidget.listItem}`);
    let amountOfAllItems = await widgetListItems.length;
    // open settings
    await Widget.openCloseSettings(page, widgetContainer);
    // disable exchanges in settings
    await Widget.disableMultiselectDropdownItems(page, widgetContainer, settingExchanges, 4);
    await Widget.openCloseSettings(page, widgetContainer);
    let itemsAfterDisabling = await page.$$(`${widgetContainer} ${locators.scoringDashboard.scoringWidget.listItem}`);
    let amountOfItemsAfterDisabling = await itemsAfterDisabling.length;
    await expect(amountOfItemsAfterDisabling).toBe(amountOfAllItems-4);
  });

});
