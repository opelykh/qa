const ENVIRONMENTS = {
  dev: 'http://10.100.1.100',
  qa: 'http://10.100.2.100',
  prod: 'https://platform.cryptoexchangeranks.com',
  local: 'localhost:3000'
}

const PLATFORMS = {
  desktop: 'desktop',
  mobile: 'mobile'
}


const userSubscribe = {
  email: 'test@cer.com',
  name: 'Test'
}

const devices = {
  iPhone8: 'iPhone 8'
}

const BROWSER = {
  width: 1536,
  height: 809,
  slowMo: 100,
  headless: false,
  devtools: false
}

export default
{
  URL: ENVIRONMENTS.local,
  device: devices.iPhone8,
  platform: PLATFORMS.desktop,
  browserWidth: BROWSER.width,
  browserHeight: BROWSER.height,
  slowMo: BROWSER.slowMo,
  headless: BROWSER.headless,
  userSubscribe: userSubscribe,
  devtools: BROWSER.devtools
}
