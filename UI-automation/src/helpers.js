import puppeteer from 'puppeteer';
import config from './config/config.js';
const width = config.browserWidth;
const height = config.browserHeight;
const device = config.device;

export default {
  wait: function (ms) {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
  },

  setupBrowser: async function() {
    const browser = await puppeteer.launch({
      headless: config.headless,
      slowMo: config.slowMo,
      args: [`--window-size=${width},${height}`],
      devtools: config.devtools
    });
    return browser;
  },

  setupPage: async function(browser) {
    let page = await browser.newPage();
    await page.setViewport({
      width,
      height
    });
    await page.goto(config.URL);
    return page;
  }

}
